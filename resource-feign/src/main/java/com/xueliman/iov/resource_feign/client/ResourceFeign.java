package com.xueliman.iov.resource_feign.client;

import com.xueliman.iov.auth_server_feign_common.config.FeignRequestConfig;
import com.xueliman.iov.cloud.framework.web.exception.ApiException;
import com.xueliman.iov.resource_feign.dto.TestDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "resource", fallbackFactory = ResourceFeign.ResourceFallback.class, configuration = FeignRequestConfig.class)
public interface ResourceFeign {

    @GetMapping(value = "/getResourceTest")
    String getResourceTest();

    @GetMapping(value = "/getResourceTest2")
    String getResourceTest2();

    @GetMapping(value = "/getResourceTest3")
    Integer getResourceTest3(@RequestBody TestDTO testDTO);


    @Slf4j
    @Component
    class ResourceFallback implements FallbackFactory<ResourceFeign> {
        @Override
        public ResourceFeign create(Throwable cause) {

            return new ResourceFeign() {
                @Override
                public String getResourceTest() {
                    return "接口请求异常";
                }

                @Override
                public String getResourceTest2() {
                    if (cause instanceof ApiException){
                        throw new ApiException(cause.getMessage());
//                        return cause.getMessage();
                    }else {
                        return "接口请求异常";
                    }
                }

                @Override
                public Integer getResourceTest3(TestDTO testDTO) {
                    if (cause instanceof ApiException){
                        String message = cause.getMessage();
                        throw new ApiException(message);
                    }else {
                        return null;
                    }
                }
            };
        }
    }

}
